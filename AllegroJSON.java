package allegrss;

import org.json.simple.JSONObject;

/**
 * Interface which holds import/export from/to JSON and exception used to implement
 * JSON utilities in AllegroItem (and derived) and AllegroQuery classes.
 *
 * @author bruteus
 */
public interface AllegroJSON {

    /**
     * This exception may be thrown if specified JSONObject is not in valid form.
     * What means "valid" in this case can be interpreted freely.
     */
    public final class InvalidJSONObjectException extends Exception {
        private final JSONObject invalidObject;
        public InvalidJSONObjectException(JSONObject obj) {
            this.invalidObject = obj;
        }

        public JSONObject getInvalidObject() {
            return this.invalidObject;
        }
    }
    static final Exception InvalidJSONObject = new Exception("Invalid JSONObject specified.");

    JSONObject exportToJSON();
    void importFromJSON(JSONObject data) throws Exception;
}