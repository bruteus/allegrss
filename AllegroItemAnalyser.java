package allegrss;

import java.net.MalformedURLException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * This class has methods to fill all fields in AllegroBasicItem and AllegroDetailedItem.
 *
 * @author bruteus
 */
public class AllegroItemAnalyser {
    private final Elements extractedItems;

    public AllegroItemAnalyser(Elements extracted) {
        this.extractedItems = extracted;
    }

    /**
     * This function constructs a new AllegroBasicItem object from information
     * exctracted from given HTML doc.
     *
     * @param doc HTML document which holds information of a single item from allegro.pl listing
     * @return Constructed AllegroBasicItem instance
     * @throws MalformedURLException When any URL in given doc was invalid
     */
    public static AllegroBasicItem getBasicInformation(Document doc) throws MalformedURLException {
        AllegroBasicItem basicItem = new AllegroBasicItem();
        AllegroBasicItemAnalyser basicAnalyser = new AllegroBasicItemAnalyser(doc);

        basicItem.setTitle(basicAnalyser.getTitle());
        basicItem.setLink(basicAnalyser.getLink());
        basicItem.setImages(basicAnalyser.getImages());
        basicItem.setParameters(basicAnalyser.getParameters());
        basicItem.setPrice(basicAnalyser.getPrice());
        basicItem.setShippingPrice(basicAnalyser.getShippingPrice());
        basicItem.setSellerType(basicAnalyser.getSellerType());
        basicItem.setOfferType(basicAnalyser.getOfferType());
        basicItem.setAdditionalOfferInfo(basicAnalyser.getAdditionalOfferInfo());

        return basicItem;
    }

    /**
     * This function constructs a new AllegroBasicItem object from information
     * exctracted from given HTML doc.
     *
     * @param doc HTML document which holds information of a single item from this item's webpage on allegro
     * @return Constructed AllegroDetailedItem instance
     * @throws MalformedURLException When any URL in given doc was invalid
     */
    public static AllegroDetailedItem getDetailedInformation(Document doc) throws MalformedURLException {
        AllegroDetailedItem detailedItem = new AllegroDetailedItem();
        AllegroDetailedItemAnalyser detailedAnalyser = new AllegroDetailedItemAnalyser(doc);

        detailedItem.setTitle(detailedAnalyser.getTitle());
        detailedItem.setLink(detailedAnalyser.getLink());
        detailedItem.setImages(detailedAnalyser.getImages());
        detailedItem.setParameters(detailedAnalyser.getParameters());
        detailedItem.setPrice(detailedAnalyser.getPrice());
        detailedItem.setShippingPrice(detailedAnalyser.getShippingPrice());
        detailedItem.setSellerType(detailedAnalyser.getSellerType());
        detailedItem.setOfferType(detailedAnalyser.getOfferType());
        detailedItem.setAdditionalOfferInfo(detailedAnalyser.getAdditionalOfferInfo());
        detailedItem.setSeller(detailedAnalyser.getSeller());
        detailedItem.setSellerPositiveComments(detailedAnalyser.getSellerPositiveComments());
        detailedItem.setShippingVariants(detailedAnalyser.getShippingVariants());
        detailedItem.setDescription(detailedAnalyser.getDescription());

        return detailedItem;
    }

    /**
     * Convinience function which performs getBasicInformation function on each Element in extractedItems field.
     *
     * @return Array of constructed AllegroBasicItems
     * @throws MalformedURLException When any URL in given docs was invalid
     */
    public ArrayList<AllegroBasicItem> getAllBasicInformation() throws MalformedURLException {
        ArrayList<AllegroBasicItem> basicItems = new ArrayList<>();
        for(Element e: this.extractedItems) {
            Document d = Jsoup.parse(e.html());
            basicItems.add(AllegroItemAnalyser.getBasicInformation(d));
        }

        return basicItems;
    }
}