package allegrss;

import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class provides methods to load config files content and store it in variables.
 * 
 * @author bruteus
 */
public final class AllegroConfig {
    public static String CONFIG_FILE_NAME = "/home/bruteus/Documents/allegrss/config.json";
    
    private static ArrayList<String> basicItemRequiredKeys;
    private static ArrayList<String> detailedItemRequiredKeys;
    private static HashMap<String, String> basicItemSelectors;
    private static HashMap<String, String> detailedItemSelectors;
    
    public static void importItemsConfig() throws IOException, ParseException {
        AllegroConfig.basicItemRequiredKeys = AllegroConfig.importItemRequiredKeys("basic_item");
        AllegroConfig.detailedItemRequiredKeys = AllegroConfig.importItemRequiredKeys("detailed_item");
        
        AllegroConfig.basicItemSelectors = AllegroConfig.importItemSelectors("basic_item");
        AllegroConfig.detailedItemSelectors = AllegroConfig.importItemSelectors("detailed_item");
    }
    
    private static ArrayList<String> importItemRequiredKeys(String configKey) throws IOException, ParseException {
        ArrayList<String> requiredKeys = new ArrayList<>();

        JSONParser parser = new JSONParser();
        JSONObject root = (JSONObject) parser.parse(new FileReader(CONFIG_FILE_NAME));
        JSONObject config = (JSONObject) root.get(configKey);
        JSONArray reqKeys = (JSONArray) config.get("required_keys");
        for(Object k: reqKeys)
            requiredKeys.add((String) k);
        
        return requiredKeys;
    }
    
    @SuppressWarnings("unchecked")
    private static HashMap<String, String> importItemSelectors(String configKey) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject root = (JSONObject) parser.parse(new FileReader(CONFIG_FILE_NAME));
        JSONObject config = (JSONObject) root.get(configKey);
        
        return (HashMap<String, String>) config.get("selectors");
    }
    
    // Getters:
    public static ArrayList<String> getBasicItemRequiredKeys() { return AllegroConfig.basicItemRequiredKeys; }
    public static ArrayList<String> getDetailedItemRequiredKeys() { return AllegroConfig.detailedItemRequiredKeys; }
    public static HashMap<String, String> getBasicItemSelectors() { return AllegroConfig.basicItemSelectors; }
    public static HashMap<String, String> getDetailedItemSelectors() { return AllegroConfig.detailedItemSelectors; }
}
