package allegrss;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.HashMap;

import org.json.simple.JSONObject;

/**
 * This class represents a query for allegro services and provides utilities defined in
 * AllegroJSON interface for its instances.
 *
 * @author bruteus
 */
public final class AllegroQuery implements AllegroJSON {
    private final String URL_BASE = "https://allegro.pl/listing?string=";
    public String label;
    public URL link;
    public boolean updateAtStartup;
    public int updateInterval;

    public AllegroQuery() {
        label = null;
        link = null;
        updateAtStartup = false;
        updateInterval = -1;
    }

    public AllegroQuery(String l, String q, boolean uas, int ui) throws MalformedURLException {
        this.label = l;
        this.link = new URL(URL_BASE + q);
        this.updateAtStartup = uas;
        this.updateInterval = ui;
    }

    public AllegroQuery(String l, URL u, boolean uas, int ui) {
        this.label = l;
        this.link = u;
        this.updateAtStartup = uas;
        this.updateInterval = ui;
    }

    /**
     * Exports all data from class into a JSONObject variable.
     *
     * @return Constructed JSONObject instance
     */
    @Override
    @SuppressWarnings("unchecked")
    public JSONObject exportToJSON() {
        JSONObject jsonExported = new JSONObject();
        jsonExported.put("label", label);
        jsonExported.put("link", link.toString());
        jsonExported.put("startup", (updateAtStartup ? 1 : 0));
        jsonExported.put("interval", updateInterval);

        return jsonExported;
    }

    /**
     * Takes provided JSONObject and imports its content into class fields.
     * If parameter doesn't contain required keys, it will throw an error.
     *
     * @param data JSONObject which holds required data to fill class fields
     * @throws InvalidJSONObjectException
     * @throws MalformedURLException
     */
    @Override
    public void importFromJSON(JSONObject data) throws InvalidJSONObjectException, MalformedURLException {
        HashMap<String, Object> extractedData = new HashMap<>();

        if(!(data.containsKey("label") && data.containsKey("link") && data.containsKey("startup") && data.containsKey("interval"))) {
            throw new InvalidJSONObjectException(data);
        }

        label = (String) data.get("label");
        link = new URL((String) data.get("link"));

        try {
            updateAtStartup = Integer.parseInt((String) data.get("startup")) == 1;
            updateInterval = Integer.parseInt((String) data.get("interval"));
        }
        catch (Exception exception) {
            System.out.println("While creating new AllegroQuery from JSON data:\n\t" + exception.toString());
            throw new InvalidJSONObjectException(data);
        }
    }
}
