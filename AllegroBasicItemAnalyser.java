package allegrss;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * This class serves as a analyser for HTML docs which hold information suitable
 * for AllegroBasicItem class.
 *
 * @author bruteus
 */
public class AllegroBasicItemAnalyser implements AllegroBasicAnalyserInterface {

    /**
     * Variable which holds extracted HTML from allegro. It should contain only information
     * of SINGLE item found in the listing.
     */
    protected final Document doc;

    // Selectors used by jsoup in order to extract specific information
    protected String TITLE_SELECTOR;
    protected String LINK_SELECTOR;
    protected String IMAGES_SELECTOR;
    protected String PARAMETERS_SELECTOR;
    protected String PRICE_SELECTOR;
    protected String SHIPPING_PRICE_SELECTOR;
    protected String SELLER_TYPE_SUPER_SELLER_SELECTOR;
    protected String OFFER_TYPE_BUY_NOW_SELECTOR;
    protected String OFFER_TYPE_AUCTION_WITH_BUY_NOW_OPTION_SELECTOR;
    protected String ADDITIONAL_OFFER_INFO_BIDS_SELECTOR;
    protected String ADDITIONAL_OFFER_INFO_TIMES_BOUGHT_SELECTOR;
    protected String ADDITIONAL_OFFER_INFO_REMAINING_SELECTOR;
    protected String ADDITIONAL_OFFER_INFO_BUY_NOW_OPTIONAL_SELECTOR;

    public AllegroBasicItemAnalyser(Document d) {
        this.doc = d;

        // Note: If AllegroConfig basicItemSelectors isn't initialized, it will throw an error
        HashMap<String, String> selectors = AllegroConfig.getBasicItemSelectors();
        // Initializing selector fields with information from config file
        TITLE_SELECTOR = selectors.get("title");
        LINK_SELECTOR = selectors.get("link");
        IMAGES_SELECTOR = selectors.get("images");
        PARAMETERS_SELECTOR = selectors.get("parameters");
        PRICE_SELECTOR = selectors.get("price");
        SHIPPING_PRICE_SELECTOR = selectors.get("shipping_price");
        SELLER_TYPE_SUPER_SELLER_SELECTOR = selectors.get("seller_type_super_seller");
        OFFER_TYPE_BUY_NOW_SELECTOR = selectors.get("offer_type_buy_now");
        OFFER_TYPE_AUCTION_WITH_BUY_NOW_OPTION_SELECTOR = selectors.get("offer_type_auction_with_buy_now_option");
        ADDITIONAL_OFFER_INFO_BIDS_SELECTOR = selectors.get("additional_offer_info_bids");
        ADDITIONAL_OFFER_INFO_TIMES_BOUGHT_SELECTOR = selectors.get("additional_offer_info_times_bought");
        ADDITIONAL_OFFER_INFO_REMAINING_SELECTOR = selectors.get("additional_offer_info_remaining");
        ADDITIONAL_OFFER_INFO_BUY_NOW_OPTIONAL_SELECTOR = selectors.get("additional_offer_info_buy_now_optional");
    }

    /**
     * This function parses a number and returns its real representation.
     * An exception is thrown when given string doesn't hold any number at all.
     *
     * @param numText A string which contain single number
     * @return Parsed number
     */
    protected final double parseAllegroNumber(String numText) {
        float number;
        numText = numText.replace(",", "."); // Allegro uses comma as separator
        numText = numText.replace(" ", ""); // Allegro puts spaces in numbers above 1000 (1 000)

        try {
            Pattern numberPattern = Pattern.compile("\\d+(\\.\\d+)?");
            Matcher matcher = numberPattern.matcher(numText);
            matcher.find();

            number = Float.parseFloat(matcher.group());
        }
        catch (Exception exception) {
            throw new AnalyserInvaldNumberFormatException(this.doc, numText);
        }

        return number;
    }

    protected final void nullCheck(Element e) {
        if(e == null) {
            throw new AnalyserFieldNotFoundException(this.doc);
        }
    }

    protected final void emptyCheck(Element e) {
        try {
            if(!e.hasText())
                throw new AnalyserEmptyFieldException(this.doc);
        }
        catch (NullPointerException exception) {
            throw new AnalyserFieldNotFoundException(this.doc);
        }
    }

    protected final boolean emptyCheckNoThrow(Element e) {
        try {
            return !e.hasText();
        }
        catch (NullPointerException exception) {
            return true;
        }
    }

    @Override
    public String getTitle() {
        Element titleHTML = this.doc.select(TITLE_SELECTOR).first();
        this.emptyCheck(titleHTML);

        return titleHTML.text();
    }

    @Override
    public URL getLink() throws MalformedURLException {
        Element linkHTML = this.doc.select(LINK_SELECTOR).first();
        this.nullCheck(linkHTML);

        return new URL(linkHTML.attr("href"));
    }

    @Override
    public ArrayList<URL> getImages() throws MalformedURLException {
        ArrayList<URL> images = new ArrayList<>();
        Element imagesHTML = this.doc.select(IMAGES_SELECTOR).first();
        this.nullCheck(imagesHTML);

        images.add(new URL(imagesHTML.attr("src")));

        return images;
    }

    @Override
    public HashMap<String, String> getParameters() {
        HashMap<String, String> parameters = new HashMap<>(); // Can be null if there is no parameters at all
        Elements parametersHTML = this.doc.select(PARAMETERS_SELECTOR);

        for(int i = 0; i < parametersHTML.size()-1; i += 2) { // This won't throw exception if parametersHTML is null
            this.emptyCheck(parametersHTML.get(i));
            this.emptyCheck(parametersHTML.get(i+1));
            String k = parametersHTML.get(i).text();
            String v = parametersHTML.get(i+1).text();

            parameters.put(k, v);
        }

        return parameters;
    }

    @Override
    public double getPrice() {
        Element priceHTML = this.doc.select(PRICE_SELECTOR).first();
        this.emptyCheck(priceHTML);

        return this.parseAllegroNumber(priceHTML.text());
    }

    @Override
    public double getShippingPrice() {
        Element shippingPriceHTML = this.doc.select(SHIPPING_PRICE_SELECTOR).first();
        if(this.emptyCheckNoThrow(shippingPriceHTML)) // If there is only "personal collection" shipping option Allegro doesn't show shipping price
            return 0;

        if(shippingPriceHTML.text().startsWith("darmowa")) // Free shipping
            return 0;

        return this.parseAllegroNumber(shippingPriceHTML.text());
    }

    @Override
    public AllegroItem.SellerType getSellerType() {
        Element sellerTypeSuperSellerHTML = this.doc.select(SELLER_TYPE_SUPER_SELLER_SELECTOR).first();
        if(sellerTypeSuperSellerHTML == null) { // Null value in this case may also means that allegro changed something in their website
            return AllegroItem.SellerType.normal;
        }
        else {
            return AllegroItem.SellerType.super_seller;
        }
    }

    @Override
    public AllegroItem.OfferType getOfferType() {
        Element offerTypeBuyNowHTML = this.doc.select(OFFER_TYPE_BUY_NOW_SELECTOR).first();
        Element offerTypeAuctionWithBuyNowOptionHTML = this.doc.select(OFFER_TYPE_AUCTION_WITH_BUY_NOW_OPTION_SELECTOR).first();

        if(offerTypeBuyNowHTML != null) // Null value in this case may also means that allegro changed something in their website
            return AllegroItem.OfferType.buyNow;
        else if(offerTypeAuctionWithBuyNowOptionHTML != null)
            return AllegroItem.OfferType.auctionWithBuyNowOption;
        else
            return AllegroItem.OfferType.auction;
    }

    @Override
    public HashMap<String, Object> getAdditionalOfferInfo() {
        HashMap<String, Object> additionalOfferInfo = new HashMap<>();
        Element bidsHTML = this.doc.select(ADDITIONAL_OFFER_INFO_BIDS_SELECTOR).first();
        Element timesBoughtHTML = this.doc.select(ADDITIONAL_OFFER_INFO_TIMES_BOUGHT_SELECTOR).first();
        Element remainingHTML = this.doc.select(ADDITIONAL_OFFER_INFO_REMAINING_SELECTOR).first();
        Element buyNowOptionalHTML = this.doc.select(ADDITIONAL_OFFER_INFO_BUY_NOW_OPTIONAL_SELECTOR).first();

        if(!this.emptyCheckNoThrow(bidsHTML)) { // Auction
            if(bidsHTML.text().startsWith("nikt"))
                additionalOfferInfo.put("bids", 0);
            else
                additionalOfferInfo.put("bids", this.parseAllegroNumber(bidsHTML.text()));

            if(!this.emptyCheckNoThrow(remainingHTML))
                additionalOfferInfo.put("remaining", this.parseAllegroNumber(remainingHTML.text()));

            if(!this.emptyCheckNoThrow(buyNowOptionalHTML)) {
                additionalOfferInfo.put("buy_now_optional", this.parseAllegroNumber(buyNowOptionalHTML.text()));
            }
        }
        else { // Buy now
            if(!this.emptyCheckNoThrow(timesBoughtHTML)) {
                additionalOfferInfo.put("times_bought", this.parseAllegroNumber(timesBoughtHTML.text()));
            }
            else {
                additionalOfferInfo.put("times_bought", 0);
            }
        }

        return additionalOfferInfo;
    }
}