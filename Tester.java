package allegrss;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Tester {
    public static void main(String[] args) throws Exception {
        AllegroConfig.importItemsConfig();

        AllegroItemExtractor extractor = new AllegroItemExtractor("/home/bruteus/Documents/allegrss/zapytania.txt");
        ArrayList<Elements> el = extractor.getAllItems();
        ArrayList<AllegroItem> items = new ArrayList<>();

        for(Elements e: el) {
            //System.out.println("\n\nNext set...\n\n");
            AllegroItemAnalyser analyzer = new AllegroItemAnalyser(e);
            for(AllegroBasicItem ai: analyzer.getAllBasicInformation()) {
                //System.out.println("\nNext item...");
                //System.out.println(ai.exportToJSON());
                items.add(ai);
            }
        }

        AllegroBasicItem firstBasic = (AllegroBasicItem) items.get(0);
        URL firstLink = firstBasic.getLink();
        Document doc = Jsoup.connect(firstLink.toString()).get();
        AllegroItemAnalyser anal = new AllegroItemAnalyser(null);
        AllegroDetailedItem first = anal.getDetailedInformation(doc);
        //System.out.println(first.exportToJSON());

        String firstExported = first.exportToJSON().toJSONString();

        String path = "/tmp/export.json";
        FileWriter writer = new FileWriter(path);
        writer.write(first.exportToJSON().toJSONString());
        writer.close();

        AllegroDetailedItem second = new AllegroDetailedItem();

        JSONParser parser = new JSONParser();
        JSONObject basicObj = (JSONObject) parser.parse(new FileReader(path));
        second.importFromJSON(basicObj);
        //System.out.println(second.exportToJSON());
        String secondExported = second.exportToJSON().toJSONString();

        if(firstExported.equals(secondExported))
            System.out.println("It works");
        else
            System.out.println("It ain't work");
    }
}