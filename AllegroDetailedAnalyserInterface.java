package allegrss;

import java.util.HashMap;

/**
 * This interface holds some methods specific only to AllegroDetailedAnalyser.
 *
 * @author bruteus
 */
public interface AllegroDetailedAnalyserInterface extends AllegroBasicAnalyserInterface {
    String getSeller();
    double getSellerPositiveComments();
    HashMap<String, Double> getShippingVariants();
    String getDescription();
}