# AllegRSS

Allegro zakończyło wsparcie dla kanałów RSS. **AllegRSS** ma w założeniu być zamiennikiem dla tej usługi,
aby użytkownicy polegający w przeszłości na RSSach nie musieli się zbytnio wysilać i szukać innych rozwiązań.
