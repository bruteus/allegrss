package allegrss;

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * This class takes a listing of items downloaded from allegro and extracts each one
 * of them. To download such listings it uses information specified in JSON file with
 * AllegroQueries.
 *
 * @author bruteus
 */
public class AllegroItemExtractor {
    public static final String ITEM_CLASS_SELECTOR = "div.ff162b8";
    public ArrayList<AllegroQuery> queries;

    /**
     * Takes information from specified file, forms AllegroQuery instances from it
     * and put them into queries class variable.
     *
     * @param sourceFilePath JSON file with exported AllegroQueries
     * @throws FileNotFoundException
     * @throws ParseException
     * @throws IOException
     * @throws AllegroJSON.InvalidJSONObjectException
     */
    public AllegroItemExtractor(String sourceFilePath) throws FileNotFoundException, ParseException, IOException, AllegroJSON.InvalidJSONObjectException {
        this.queries = new ArrayList<>();
        JSONParser parser = new JSONParser();
        JSONObject queriesJSON = (JSONObject) parser.parse(new FileReader(sourceFilePath));
        JSONArray queriesArray = (JSONArray) ((JSONObject) queriesJSON.get("menu")).get("menuitem");
        for (Object obj: queriesArray) {
            AllegroQuery q = new AllegroQuery();
            q.importFromJSON((JSONObject) obj);
            this.queries.add(q);
        }
    }

    /**
     * Function which extract all items from single listing from URL specified in AllegroQuery parameter.
     *
     * @param aq AllegroQuery object that contains URL to listing at allegro
     * @return All items from listing
     * @throws IOException
     */
    public static Elements getItems(AllegroQuery aq) throws IOException {
        Document doc = Jsoup.connect(aq.link.toString()).get();
        Elements items = doc.select(ITEM_CLASS_SELECTOR);

        return items;
    }

    /**
     * Convinience function which returns all items from all listings which URLs are specified
     * in queries class list.
     *
     * @return All items from all listings
     * @throws IOException
     */
    public ArrayList<Elements> getAllItems() throws IOException {
        ArrayList<Elements> allItems = new ArrayList<>();
        for(AllegroQuery aq: this.queries) {
            allItems.add(getItems(aq));
        }

        return allItems;
    }
}