package allegrss;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.nodes.Document;

/**
 * Interface for AllegroBasicAnalyser.
 *
 * It holds all functions needed to extract certain information for AllegroBasicItem class.
 * Some runtime exceptions are also inside in order to provide easier debugging process.
 *
 * @author bruteus
 */
public interface AllegroBasicAnalyserInterface {

    /**
     * When this exception is thrown it probably means that allegro team changed some
     * class names in their website.
     */
    public class AnalyserFieldNotFoundException extends RuntimeException {
        protected final Document currentHTML;
        public AnalyserFieldNotFoundException(Document chtml) {
            currentHTML = chtml;
        }

        public Document getCurrentHTML() {
            return currentHTML;
        }
    }

    /**
     * This exception may be thrown when the requested information is found in HTML doc,
     * but doesn't hold any value.
     */
    public final class AnalyserEmptyFieldException extends AnalyserFieldNotFoundException {
        public AnalyserEmptyFieldException(Document chtml) {
            super(chtml);
        }
    }

    /**
     * This exception can be thrown if number string passed to parseAllegroNumber is in
     * invalid format.
     */
    public final class AnalyserInvaldNumberFormatException extends RuntimeException {
        private final Document currentHTML;
        private final String numberString;
        public AnalyserInvaldNumberFormatException(Document chtml, String ns) {
            currentHTML = chtml;
            numberString = ns;
        }

        public Document getCurrentHTML() {
            return currentHTML;
        }

        public String getNumberString() {
            return numberString;
        }
    }

    // Functions that extracts information form HTML doc
    String getTitle();
    URL getLink() throws MalformedURLException;
    ArrayList<URL> getImages() throws MalformedURLException;
    HashMap<String, String> getParameters();
    double getPrice();
    double getShippingPrice();
    AllegroItem.SellerType getSellerType();
    AllegroItem.OfferType getOfferType();
    HashMap<String, Object> getAdditionalOfferInfo();
}