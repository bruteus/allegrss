package allegrss;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class serves mainly as a container for all information provided by Allegro
 * for single item visible in listing (after searching on allegro.pl).
 *
 * @author bruteus
 */
public class AllegroBasicItem extends AllegroItem {

    /**
     * JSON keys that need to be in config file in order to importFromJSON function work properly
     */
    protected ArrayList<String> requiredKeys;

    // Fields for information extracted from HTML
    protected String title;
    protected URL link;
    protected ArrayList<URL> images;
    protected HashMap<String, String> parameters;
    protected double price;
    protected double shippingPrice;
    protected SellerType sellerType;
    protected OfferType offerType;
    protected HashMap<String, Object> additionalOfferInfo;

    public AllegroBasicItem() {
        // Default values
        title = null;
        link = null;
        images = null;
        parameters = null;
        price = -1.0;
        shippingPrice = -1.0;
        sellerType = SellerType.unknown;
        offerType = OfferType.unknown;
        additionalOfferInfo = null;

        // Note: If AllegroConfig basicItemRequiredKeys isn't initialized, it will throw an error
        requiredKeys = AllegroConfig.getBasicItemRequiredKeys();
    }

    /**
     * Imports data from given JSONObject and assings it to proper class fields.
     *
     * If supplied JSONObject doesn't contain required keys specified in requiredKeys list
     * an error is thrown.
     *
     * @param data
     * @throws InvalidJSONObjectException
     * @throws MalformedURLException
     */
    @Override
    @SuppressWarnings("unchecked")
    public void importFromJSON(JSONObject data) throws InvalidJSONObjectException, MalformedURLException {
        for(String s: this.requiredKeys) {
            if(!data.containsKey(s))
                throw new InvalidJSONObjectException(data);
        }

        // Setting class variables with imported data
        this.setTitle((String) data.get("title"));
        this.setLink(new URL((String) data.get("link")));

        JSONArray imagesArray = (JSONArray) data.get("images");
        ArrayList<URL> img = new ArrayList<>();
        for(Object s: imagesArray)
            img.add(new URL((String) s));
        this.setImages(img);

        this.setParameters((HashMap<String, String>) data.get("parameters"));

        String sellerTypeExtracted = (String) data.get("seller_type");
        if(sellerTypeExtracted.equals(SellerType.normal.toString()))
            this.setSellerType(SellerType.normal);
        else if(sellerTypeExtracted.equals(SellerType.super_seller.toString()))
            this.setSellerType(SellerType.super_seller);
        else
            throw new InvalidJSONObjectException(data);

        String offerTypeExtracted = (String) data.get("offer_type");
        if(offerTypeExtracted.equals(OfferType.auction.toString()))
            this.setOfferType(OfferType.auction);
        else if(offerTypeExtracted.equals(OfferType.buyNow.toString()))
            this.setOfferType(OfferType.buyNow);
        else
            throw new InvalidJSONObjectException(data);

        this.setPrice((double) data.get("price"));
        this.setShippingPrice((double) data.get("shipping_price"));

        this.setAdditionalOfferInfo((HashMap<String, Object>) data.get("additional_offer_info"));
    }

    /**
     * This function exports all data from class (except requiredKeys list) into a JSONObject variable
     * @return JSONObject variable with all information extracted from AllegroBasicItem class
     */
    @Override
    @SuppressWarnings("unchecked")
    public JSONObject exportToJSON() {
        JSONObject jsonExported = new JSONObject();

        jsonExported.put("title", title);
        jsonExported.put("link", link.toString());

        JSONArray imgArray = new JSONArray();
        for(URL u: this.images)
            imgArray.add(u.toString());
        jsonExported.put("images", imgArray);

        jsonExported.put("parameters", parameters);
        jsonExported.put("price", price);
        jsonExported.put("shipping_price", shippingPrice);
        jsonExported.put("seller_type", sellerType.toString());
        jsonExported.put("offer_type", offerType.toString());
        jsonExported.put("additional_offer_info", additionalOfferInfo);

        return jsonExported;
    }

    // Getters
    public final String getTitle() { return this.title; }
    public final URL getLink() { return this.link; }
    public final ArrayList<URL> getImages() { return this.images; }
    public final HashMap<String, String> getParameters() { return this.parameters; }
    public final double getPrice() { return this.price; }
    public final double getShippingPrice() { return this.shippingPrice; }
    public final SellerType getSellerType() { return this.sellerType; }
    public final OfferType getOfferType() { return this.offerType; }
    public final HashMap<String, Object> getAdditionalOfferInfo() { return this.additionalOfferInfo; }

    // Setters
    public final void setTitle(String arg) {
        if(!arg.isEmpty())
            this.title = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Title is null or empty");
    }

    public final void setLink(URL arg) {
        String linkText = arg.toString();
        if(linkText.contains("allegro.pl/"))
            this.link = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Invalid link");
    }

    public final void setImages(ArrayList<URL> arg) {
        if(arg == null)
            throw new AllegroItemInvalidSetterValue(arg, "Images list is null");

        for(URL u: arg) {
            if(u == null)
                throw new AllegroItemInvalidSetterValue(arg, "One image link is null");
            if(!u.toString().contains(".allegroimg.com/") && !u.toString().contains("assets.allegrostatic.com/"))
                throw new AllegroItemInvalidSetterValue(arg, "One image link is invalid");
        }

        this.images = arg;
    }

    public final void setParameters(HashMap<String, String> arg) {
        if(arg == null)
            throw new AllegroItemInvalidSetterValue(arg, "Parameters map is null");

        for(Map.Entry e: arg.entrySet()) {
            if(((String) e.getKey()).isEmpty() || ((String) e.getValue()).isEmpty())
                throw new AllegroItemInvalidSetterValue(arg, "One of the parameters in map is null or empty");
        }

        this.parameters = arg;
    }

    public final void setPrice(double arg) {
        if(arg > 0)
            this.price = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Price is less or equal to zero");
    }

    public final void setShippingPrice(double arg) {
        if(this.price <= arg && this.price > 0)
            this.shippingPrice = arg - this.price;
        else if(arg == 0)
            this.shippingPrice = 0;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Shipping price doesn't meet requirements");
    }

    public final void setSellerType(SellerType arg) {
        if(arg != null && arg != SellerType.unknown)
            this.sellerType = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "SellerType shouldn't be null or unknown");
    }

    public final void setOfferType(OfferType arg) {
        if(arg != null && arg != OfferType.unknown)
            this.offerType = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "OfferType shouldn't be null or unknown");
    }

    public final void setAdditionalOfferInfo(HashMap<String, Object> arg) {
        if(arg == null)
            throw new AllegroItemInvalidSetterValue(arg, "Additional offer info shouldn't be null");

        for(Map.Entry e: arg.entrySet()) {
            if(((String) e.getKey()).isEmpty())
                throw new AllegroItemInvalidSetterValue(arg, "Key in additional offer info empty or null");
            if(e.getValue() == null)
                throw new AllegroItemInvalidSetterValue(arg, "Value in additional offer info is null");
        }

        this.additionalOfferInfo = arg;
    }
}