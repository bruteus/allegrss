package allegrss;

import java.net.MalformedURLException;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

/**
 * This class holds all information extracted from item's own website on allegro (not from listing).
 *
 * @author bruteus
 */
public final class AllegroDetailedItem extends AllegroBasicItem {

    // Information extracted from HTML
    private String seller;
    private double sellerPositiveComments;
    private HashMap<String, Double> shippingVariants;
    private String description;
    // new fields may (dis)appear in the future if needed

    public AllegroDetailedItem() {
        // Assigning default values
        super();
        seller = null;
        sellerPositiveComments = -1.0;
        shippingVariants = null;
        description = null;

        // Note: If AllegroConfig detailedItemRequiredKeys isn't initialized, it will throw an error
        requiredKeys = AllegroConfig.getDetailedItemRequiredKeys();
    }

    @Override
    @SuppressWarnings("unchecked")
    public final void importFromJSON(JSONObject data) throws InvalidJSONObjectException, MalformedURLException {
        super.importFromJSON(data);

        this.setSeller((String) data.get("seller"));
        this.setSellerPositiveComments((double) data.get("seller_positive_comments"));
        this.setShippingVariants((HashMap<String, Double>) data.get("shipping_variants"));
        this.setDescription((String) data.get("description"));
    }

    @Override
    @SuppressWarnings("unchecked")
    public final JSONObject exportToJSON() {
        JSONObject jsonExported = super.exportToJSON();

        jsonExported.put("seller", seller);
        jsonExported.put("seller_positive_comments", sellerPositiveComments);
        jsonExported.put("shipping_variants", shippingVariants);
        jsonExported.put("description", description);

        return jsonExported;
    }

    // Getters
    public final String getSeller() { return this.seller; }
    public final double getSellerPositiveComments() { return this.sellerPositiveComments; }
    public final HashMap<String, Double> getShippingVariants() { return this.shippingVariants; }
    public final String getDescription() { return this.description; }

    // Setters
    public final void setSeller(String arg) {
        if(!arg.isEmpty())
            this.seller = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Seller shouldn't be either null or empty");
    }

    public final void setSellerPositiveComments(double arg) {
        if(arg >= 0 && arg <= 100)
            this.sellerPositiveComments = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Seller positive comments should fit in <0, 100>");
    }

    public final void setShippingVariants(HashMap<String, Double> arg) {
        if(arg == null)
            throw new AllegroItemInvalidSetterValue(arg, "Shipping variants map shouldn't be null");

        for(Map.Entry e: arg.entrySet()) {
            if(((String) e.getKey()).isEmpty())
                throw new AllegroItemInvalidSetterValue(arg, "One of shipping variant name empty or null");
            if(((float) e.getValue()) < 0)
                throw new AllegroItemInvalidSetterValue(arg, "One of shipping variant price below zero");
        }

        this.shippingVariants = arg;
    }

    public final void setDescription(String arg) {
        if(!arg.isEmpty())
            this.description = arg;
        else
            throw new AllegroItemInvalidSetterValue(arg, "Description is null or empty");
    }
}