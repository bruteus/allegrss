package allegrss;

import java.net.MalformedURLException;
import org.json.simple.JSONObject;

/**
 * Base class for AllegroBasicItem and therefore - AllegroDetailedItem.
 * 
 * @author bruteus
 */
public abstract class AllegroItem implements AllegroJSON {
    
    /**
     * This exception may be thrown if parameter in setter functions (in derived classes) is invalid.
     */
    public final class AllegroItemInvalidSetterValue extends RuntimeException {
        private final Object invalidObject;
        private final String message;
        public AllegroItemInvalidSetterValue(Object invObj, String msg) {
            invalidObject = invObj;
            message = msg;
        }
        
        @Override
        public String getMessage() { return this.message; }
        public Object getInvalidObject() { return this.invalidObject; }
    }

    public enum OfferType {
        auction ("auction"),
        buyNow ("buy now"),
        auctionWithBuyNowOption ("auction / buy now"),
        unknown ("unknown");

        private final String name;

        OfferType(String n) {
            name = n;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum SellerType {
        super_seller ("super seller"),
        normal ("normal seller"),
        unknown ("unknown");

        private final String name;

        SellerType(String n) {
            name = n;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Override
    public abstract void importFromJSON(JSONObject data) throws InvalidJSONObjectException, MalformedURLException;

    @Override
    public abstract JSONObject exportToJSON();
}